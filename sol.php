<?php

//clase hojo puede sobreescribir datos del padre, ya que tiene preferencia en la llamada

//hereda segun visibilidad -> 
//public: accesible desde cualquier sitio
//private:solo accesible desde la propia clase
//protected: no pueden ser llamados por elementos

//clase abstracta -> no se pueden crear objetos de la clase abstracta

//metodos abstractos -> estructura sin funcionalidad -> obliga a poner el metodo en todas las clases que heredan de este

require_once "vendor/autoload.php";

use Upgrade\Poligonos\Figuras\Circulo;
use Upgrade\Poligonos\Figuras\Cuadrado;
use Upgrade\Poligonos\Figuras\Rectangulo;

$cuadrado = new Cuadrado(20);
$rectangulo = new Rectangulo(11,5);
$circulo = new Circulo(1.21);

echo $cuadrado->calcularArea()."</br>";
echo $rectangulo->calcularArea()."</br>";
echo $circulo->calcularArea()."</br>";
?>