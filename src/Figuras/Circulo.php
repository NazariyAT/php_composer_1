<?php
namespace Upgrade\Poligonos\Figuras;

use Upgrade\Poligonos\Poligono;

class Circulo extends Poligono{
    public float $radio;

    public function __construct( float $radio)
    {
        $this->radio = $radio;
    }

    public function calcularArea():float{
        return pi() * $this->radio**2;
    }
}

?>