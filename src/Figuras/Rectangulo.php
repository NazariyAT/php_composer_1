<?php
namespace Upgrade\Poligonos\Figuras;

use Upgrade\Poligonos\Poligono;

class Rectangulo extends Poligono{
    public float $altura;
    public float $base;

    public function __construct( float $altura, float $base)
    {
        $this->altura = $altura;
        $this->base = $base;
    }

    public function calcularArea():float{
        return $this->altura * $this->base;
    }
}

?>