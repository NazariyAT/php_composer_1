<?php
namespace Upgrade\Poligonos\Figuras;

use Upgrade\Poligonos\Poligono;

class Cuadrado extends Poligono{

    public float $lado;

    public function __construct( float $lado)
    {
        $this->lado = $lado;
    }

    public function calcularArea():float{
        return $this->lado**2;
    }


}

?>